Ext.define('latihan9.store.DetailPersonnel', {
   extend: 'Ext.data.Store',

    alias: 'store.detailpersonnel',
    storeId:'detailpersonnel',

    //autoLoad: true,

    fields: [
       'user_id','name', 'email', 'phone'
    ],

    proxy: {
        type: 'jsonp',
        api:{
              read: "http://localhost/MyApp_php/readDetailPersonnel.php"
        },
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    }
});
