Ext.define('latihan9.store.Personnel', {
   extend: 'Ext.data.Store',

    alias: 'store.personnel',
    storeId:'personnel',

    autoLoad: true,

    fields: [
       'user_id','name', 'email', 'phone'
    ],

    proxy: {
        type: 'jsonp',
        api:{
              read   : "http://localhost/MyApp_php/readPersonnel.php",
              update : "http://localhost/MyApp_php/updatePersonnel.php",
              destroy: "http://localhost/MyApp_php/destroyPersonnel.php",
              create : "http://localhost/MyApp_php/createPersonnel.php"
        },
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    },
    Listeners:{
        beforeload:function(store, oepration, e0pts){
            this.getProxy().setExtraParams({
                user_id: 1
            });
        }
    }
});
