/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting causes an instance of this class to be created and
 * added to the Viewport container.
 *
 * TODO - Replace the content of this view to suit the needs of your application.
 */
Ext.define('latihan9.view.main.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'app-main',

    requires: [
        'Ext.MessageBox',

        'latihan9.view.main.MainController',
        'latihan9.view.main.MainModel',
        'latihan9.view.main.List',
        'latihan9.view.main.BasicDataView',
        'latihan9.view.forms.FormPanel',
        'latihan9.view.main.Bar',
        'latihan9.view.main.List',
        'latihan9.view.main.ListBar'

    ],

    controller: 'main',
    viewModel: 'main',

    defaults: {
        tab: {
            iconAlign: 'top'
        },
        styleHtmlContent: true
    },

    tabBarPosition: 'bottom',

    items: [
              {
            docked: 'top',
            xtype : 'toolbar',
            items : [
                {
                    xtype : 'button',
                    text  : 'Read',
                    ui    : 'action',
                    scope : this,
                    listeners : {
                        tap   : 'onReadClicked'
                    }
                }
            ]
        },
         {
            title: 'Home',
            iconCls: 'x-fa fa-home',
            layout: 'fit',
            // The following grid shares a store with the classic version's grid as well!
            items: [{
                xtype : 'panel',
                layout: 'hbox',
                items : [{
                    xtype: 'mainlist',
                    flex : 2
                }, {
                     xtype: 'detail',
                     flex : 1
                },{ xtype: 'editform',
                     flex : 1
                }]
                
            }]
        },{
            title: 'Users',
            iconCls: 'x-fa fa-user',
             layout: 'fit',
            // The following grid shares a store with the classic version's grid as well!
            items: [{
                //xtype: 'detail'
            }]
        },{
            title: 'Chart',
            iconCls: 'x-fa fa-users',
            layout: 'fit',
            // The following grid shares a store with the classic version's grid as well!
            items: [{
                xtype: 'panel',
                layout: 'vbox',
                 items : [{
                    xtype: 'bar',
                    flex : 1
                }, {
                     xtype: 'listbar',
                     flex : 1
                 }
            ]

            }]
        },{
            title: 'Settings',
            iconCls: 'x-fa fa-cog',
            bind: {
                html: '{loremIpsum}'
            }
        }
    ]
});
