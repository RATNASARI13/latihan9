/**
 * Demonstrates a tabbed form panel. This uses a tab panel with 3 tabs - Basic, Sliders and Toolbars - each of which is
 * defined below.
 *
 * See this in action at http://dev.sencha.com/deploy/sencha-touch-2-b3/examples/kitchensink/index.html#demo/forms
 */
Ext.define('latihan9.view.forms.FormPanel', {
   extend: 'Ext.form.Panel',
    xtype: 'editform',

    shadow: true,
    cls: 'demo-solid-background',
    id: 'editform',
    items:[
            {
                xtype:'textfield',
                name: 'name',
                id: 'myname',
                label: 'Name',
                placeholder: 'Your Name',
                autoCapitalize: true,
                required: true,
                clearIcon: true

            },
            {
                xtype:'emailfield',
                name: 'email',
                id: 'myemail',
                label: 'Email',
                placeholder: 'xxx@gmail.com',
                clearIcon: true
            },
            {
                xtype:'textfield',
                name: 'phone',
                id: 'myphone',
                label: 'Phone',
                placeholder: '08xxxxxxxxxx',
                clearIcon: true
            },
            {
                xtype: 'button',
                ui: 'action',
                text: ' Save Perubahan',
                handler: 'onSimpanPerubahan'
            },
            {
                xtype: 'button',
                ui   : 'confirm',
                text : 'Tambah Personnel',
                handler : 'onTambahPersonnel'
            }
    ]
});