/**
 * This view is an example list of people.
 */
Ext.define('latihan9.view.main.ListBar', {
    extend: 'Ext.grid.Grid',
    xtype: 'listbar',

    title : 'List Bar',

    requires: [
        'latihan9.store.Personnel',
        'Ext.grid.plugin.Editable'
    ],
    plugins: [{
        type: 'grideditable'
    }],

    title: 'Personnel',

    /*store: {
        type: 'personnel'
    },*/
    bind: '{bar}',

    viewModel: {
        stores:{
            bar: {
                type: 'bar'
            }
        }
    },
    columns: [
        { text: 'Name',  dataIndex: 'name', width: 100, editable: true },
        { text: 'g1', dataIndex: 'g1', width: 230, editable: true },
        { text: 'g2', dataIndex: 'g2', width: 150, editable: true },
        { text: 'g3', dataIndex: 'g3', width: 150, editable: true },
        { text: 'g4', dataIndex: 'g4', width: 150, editable: true  },
        { text: 'g5', dataIndex: 'g5', width: 150, editable: true  },
        { text: 'g6', dataIndex: 'g6', width: 150, editable: true  }

    ],

    listeners: {
        select: 'onBarItemSelected'
    }
});
