/**
 * This view is an example list of people.
 */
Ext.define('latihan9.view.main.List', {
    extend: 'Ext.grid.Grid',
    xtype: 'mainlist',

    requires: [
        'latihan9.store.Personnel',
        'Ext.grid.plugin.Editable'
    ],
    plugins: [{
        type: 'grideditable'
    }],

    title: 'Personnel',

    /*store: {
        type: 'personnel'
    },*/
    bind: '{personnel}',

    viewModel: {
        stores:{
            personnel: {
                type: 'personnel'
            }
        }
    },
    columns: [
        { text: 'Name',  dataIndex: 'name', width: 100, editable: true },
        { text: 'Email', dataIndex: 'email', width: 230, editable: true },
        { text: 'Phone', dataIndex: 'phone', width: 150 }
    ],

    listeners: {
        select: 'onItemSelected'
    }
});
